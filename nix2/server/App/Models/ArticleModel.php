<?php

namespace App\Models;

use DolgoyAudiopunk\Framework\Models\Model;

class ArticleModel extends Model
{
    /**
     * @var string
     */
    protected string $table = 'articles';
    /**
     * @var boolean
     */
    protected bool $created_at = true;
    /**
     * @var boolean
     */
    protected bool $updated_at = true;

    /**
     * @param array<string> $array
     * @return void
     */
    public static function created(array $array): void
    {
        $articleModel = new self;
        $articleModel->insert($array);
    }

    /**
     * @param array<int>    $params
     * @param array<string> $search
     * @return void
     */
    public static function updated(array $params, array $search): void
    {
        $articleModel = new self;
        $articleModel->update($params, $search);
    }

    /**
     * @param array<int> $search
     * @return void
     */
    public static function deleted(array $search): void
    {
        $articleModel = new self;
        $articleModel->delete($search);
    }

    /**
     * @param string     $column
     * @param string     $order
     * @param array<int> $search
     * @param string     $columns
     * @param string     $join
     * @return array<array>
     */
    public static function ordered(string $column, string $order, array $search, string $columns, string $join): array
    {
        $articleModel = new self;
        return $articleModel->order($column, $order, $search, $columns, $join);
    }

    /**
     * @param array<int> $search
     * @param string     $operator
     * @param string     $columns
     * @param string     $join
     * @return array<array>
     */
    public static function getArticle(array $search, string $operator = '=', string $columns = '*', string $join = ''): array
    {
        $articleModel = new self;
        return $articleModel->where($search, $operator, $columns, $join);
    }
}
