<?php

namespace App\Models;

use DolgoyAudiopunk\Framework\Models\Model;

class UserModel extends Model
{
    /**
     * @var string
     */
    protected string $table = 'users';
    /**
     * @var boolean
     */
    protected bool $created_at = true;
    /**
     * @var boolean
     */
    protected bool $updated_at = true;

    /**
     * @param array<string> $array
     * @return void
     */
    public static function created(array $array): void
    {
        unset($array['confirmPassword']);
        $array['password'] = md5($array['password']);
        $userModel = new self;
        $userModel->insert($array);
    }

    /**
     * @param array<string> $params
     * @param array<int>    $search
     * @return void
     */
    public static function updated(array $params, array $search): void
    {
        unset($params['confirmPassword']);
        $params['password'] = md5($params['password']);
        $userModel = new self;
        $userModel->update($params, $search);
    }

    /**
     * @param array<int> $search
     * @return void
     */
    public static function deleted(array $search): void
    {
        $userModel = new self;
        $userModel->delete($search);
    }

    /**
     * @param array<string|int> $search
     * @param string            $operator
     * @param string            $columns
     * @param string            $join
     * @return array<array>
     */
    public static function getUser(array $search, string $operator = '=', string $columns = '*', string $join = ''): array
    {
        $userModel = new self;
        return $userModel->where($search, $operator, $columns, $join);
    }
}
