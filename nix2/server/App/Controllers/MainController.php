<?php

namespace App\Controllers;

class MainController extends BaseController
{
    /**
     * @return void
     */
    public function index(): void
    {
        if (isset($_SESSION['id'])) {
            header('Location: ' . $_ENV['APP_URL'] . '/list');
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/login');
        }
    }

    /**
     * @return void
     */
    public function logOut(): void
    {

        session_unset();
        header('Location: ' . $_ENV['APP_URL'] . '/login');

    }
}
