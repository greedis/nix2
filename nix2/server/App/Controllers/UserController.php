<?php

namespace App\Controllers;

use App\Models\UserModel;
use PDOException;

class UserController extends BaseController
{
    /**
     * @return boolean|null
     */
    public function list(): bool|null
    {
        if (isset($_SESSION['id'])) {
            $title = 'My office';
            $this->set(compact('title'));
            $user = $_SESSION;
            return $this->view('user/list', compact('user'));
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/login');
        }
    }

    /**
     * @return void
     */
    public function index(): void
    {
        $user = UserModel::getUser(['email' => $_POST['email'], 'password' => md5($_POST['password'])], columns: 'users.id, full_name, email, created_at, updated_at, gender, avatar', join: 'JOIN gender ON users.gender_id = gender.id');
        if ($user) {
            $user = $user[0];
            $_SESSION = $user;
            $response = [
                'status' => true,
            ];
        } else {
            $response = [
                'status' => false,
                'message' => 'Invalid email or password',
            ];
        }
        echo json_encode($response);
    }

    /**
     * @return boolean
     */
    public function register(): bool
    {
        $title = 'Register';
        $this->set(compact('title'));
        $text = 'Зарегистрироваться';
        return $this->view('user/register', ['text' => $text]);
    }

    /**
     * @return boolean
     */
    public function login(): bool
    {
        $title = 'Login';
        $this->set(compact('title'));
        $text = 'Sign in';
        return $this->view('user/login', ['text' => $text]);
    }

    /**
     * @return boolean|null
     */
    public function updateUser(): bool|null
    {
        if (isset($_SESSION['id'])) {
            $title = 'Update';

            $this->set(compact('title'));
            $user = UserModel::getUser(['users.id' => $_SESSION['id']], columns: 'users.id, full_name, email, created_at, updated_at, gender, avatar', join: 'JOIN gender ON users.gender_id = gender.id');
            return $this->view('user/update', compact('user'));
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/login');
        }
    }

    /**
     * @return void
     */
    public function create(): void
    {
        try {
            $error_fields = $this->getFields();

            if (!empty($error_fields)) {
                $response = [
                    'status' => false,
                    'message' => 'Fill in all the fields!',
                ];
            } else {
                if ($_POST['password'] === $_POST['confirmPassword']) {
                    mkdir(ROOT . '/public/images/' . $_POST['email']);
                    $path = ROOT . '/public/images/' . $_POST['email'] . '/' . $_FILES['avatar']['name'];
                    if (!move_uploaded_file($_FILES['avatar']['tmp_name'], $path)) {
                        $response = [
                            'status' => false,
                            'message' => 'Error loading avatar',
                        ];
                    } else {
                        $_POST['avatar'] = '/images/' . $_POST['email'] . '/' . $_FILES['avatar']['name'];
                        UserModel::created($_POST);
                        $response = [
                            'status' => true,
                        ];
                    }
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'Passwords do not match',
                    ];
                }
            }
            echo json_encode($response);
        } catch (PDOException $e) {
            if (stristr($e->getMessage(), 'Duplicate entry')) {
                $response = [
                    'status' => false,
                    'message' => 'This email is already in use',
                ];
                echo json_encode($response);
            }
        }
    }

    /**
     * @return void
     */
    public function update(): void
    {
        try {
            $error_fields = $this->getFields();

            if (!empty($error_fields)) {
                $response = [
                    'status' => false,
                    'message' => 'Fill in all the fields!',
                ];
            } else {
                if ($_POST['password'] === $_POST['confirmPassword']) {
                    if ($_SESSION['email'] !== $_POST['email']) {
                        rename(ROOT . '/public/images/' . $_SESSION['email'], ROOT . '/public/images/' . $_POST['email']);
                    }
                    unlink(ROOT . '/public/images/' . $_SESSION['email'] . '/' . $_FILES['avatar']['name']);
                    $path = ROOT . '/public/images/' . $_POST['email'] . '/' . $_FILES['avatar']['name'];
                    if (!move_uploaded_file($_FILES['avatar']['tmp_name'], $path)) {
                        $response = [
                            'status' => false,
                            'message' => 'Error loading avatar',
                        ];
                    } else {
                        $_POST['avatar'] = '/images/' . $_POST['email'] . '/' . $_FILES['avatar']['name'];
                        UserModel::updated($_POST, ['id' => $_SESSION['id']]);
                        $_SESSION = UserModel::getUser(['users.id' => $_SESSION['id']], columns: 'users.id, full_name, email, created_at, updated_at, gender, avatar', join: 'JOIN gender ON users.gender_id = gender.id')[0];
                        $response = [
                            'status' => true,
                        ];
                    }
                } else {
                    $response = [
                        'status' => false,
                        'message' => 'Passwords do not match',
                    ];
                }
            }
            echo json_encode($response);
        } catch (PDOException $e) {
            if (stristr($e->getMessage(), 'Duplicate entry')) {
                $response = [
                    'status' => false,
                    'message' => 'This email is already in use',
                ];
                echo json_encode($response);
            }
        }

    }

    /**
     * @return void
     */
    public function delete(): void
    {
        if (isset($_SESSION['id'])) {
            UserModel::deleted(['id' => $_SESSION['id']]);
            rmdir(ROOT . '/public/images/' . $_SESSION['email']);
            session_unset();
        }
        header('Location: ' . $_ENV['APP_URL'] . '/login');

    }

    /**
     * @return array<string>
     */
    public function getFields(): array
    {
        $error_fields = [];

        if ($_POST['full_name'] === '') {
            $error_fields[] = 'full_name';
        }

        if ($_POST['email'] === '') {
            $error_fields[] = 'email';
        }

        if ($_POST['password'] === '') {
            $error_fields[] = 'password';
        }

        if ($_POST['confirmPassword'] === '') {
            $error_fields[] = 'confirmPassword';
        }

        if (!isset($_FILES['avatar'])) {
            $error_fields[] = 'avatar';
        }
        return $error_fields;
    }
}
