<?php

namespace App\Controllers;

use App\Models\ArticleModel;

class ArticleController extends BaseController
{
    /**
     * @return boolean|null
     */
    public function viewUserArticles(): bool|null
    {
        if (isset($_SESSION['id'])) {
            $title = 'My articles';

            $this->set(compact('title'));

            $articles = ArticleModel::getArticle(['user_id' => $_SESSION['id']], columns: 'articles.id, title, description, sort, articles.created_at, email, full_name', join: 'JOIN users ON articles.user_id = users.id JOIN sorts ON articles.sort_id = sorts.id');

            return $this->view('article/list', compact('articles'));
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/login');
        }
    }

    /**
     * @return void
     */
    public function userArticles(): void
    {
        if (isset($_SESSION['id'])) {
            $articles = ArticleModel::getArticle(['user_id' => $_SESSION['id']], columns: 'articles.id, title, description, sort, articles.created_at, email, full_name', join: 'JOIN users ON articles.user_id = users.id JOIN sorts ON articles.sort_id = sorts.id');
            if (empty($articles)) {
                $response = [
                    'status' => false,
                    'message' => 'You do not have any articles yet',
                ];
            } else {
                $response = [
                    'status' => true,
                ];
            }
            echo json_encode($response);
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/login');
        }

    }

    /**
     * @return void
     */
    public function sortArticles(): void
    {
        if (isset($_SESSION['id'])) {
            $articles = ArticleModel::getArticle(['sort_id' => $_POST['sort_id']], columns: 'articles.id, title, description, sort, articles.created_at, email, full_name', join: 'JOIN users ON articles.user_id = users.id JOIN sorts ON articles.sort_id = sorts.id');
            if (empty($articles)) {
                $response = [
                    'status' => false,
                    'message' => 'There are no articles of this sort yet',
                ];
            } else {
                $response = [
                    'status' => true,
                    'sort_id' => $_POST['sort_id'],
                ];
            }
            echo json_encode($response);
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/login');
        }

    }

    /**
     * @param array<int> $params
     * @return boolean|null
     */
    public function articlesBySort(array $params): bool|null
    {
        if (isset($_SESSION['id'])) {
            $title = 'Articles';
            $sortId = $params[0];
            $this->set(compact('title'));

            $articles = ArticleModel::getArticle(['sort_id' => $sortId], columns: 'articles.id, title, description, sort, user_id, articles.created_at, email, full_name', join: 'JOIN users ON articles.user_id = users.id JOIN sorts ON articles.sort_id = sorts.id');

            return $this->view('article/list', compact('articles', 'sortId'));
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/login');
        }
    }

    /**
     * @param array<int> $params
     * @return bool|null
     */
    public function getArticleById(array $params): bool|null
    {
        if (isset($_SESSION['id'])) {
            $articleId = $params[0];
            $articleById = ArticleModel::getArticle(['articles.id' => $articleId], columns: 'articles.id, title, description, sort, user_id, articles.created_at, email, full_name', join: 'JOIN users ON articles.user_id = users.id JOIN sorts ON articles.sort_id = sorts.id');
            $title = $articleById[0]['title'];

            $this->set(compact('title'));

            return $this->view('article/articleById', compact('articleById'));
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/login');
        }
    }

    /**
     * @return boolean|null
     */
    public function writeArticle(): bool|null
    {
        if (isset($_SESSION['id'])) {
            $title = 'Article';

            $this->set(compact('title'));
            $allUserOfArticles = ArticleModel::getArticle(['user_id' => $_SESSION['id']], columns: 'articles.id, title, description, sort, articles.created_at, email', join: 'JOIN users ON articles.user_id = users.id JOIN sorts ON articles.sort_id = sorts.id');
            return $this->view('article/write', compact('allUserOfArticles'));
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/login');
        }
    }

    /**
     * @param array<int> $params
     * @return boolean|null
     */
    public function updateArticle(array $params): bool|null
    {
        if (isset($_SESSION['id'])) {
            $articleId = $params[0];
            $title = 'Update article';

            $this->set(compact('title'));
            $updatingArticle = ArticleModel::getArticle(['id' => $articleId]);
            return $this->view('article/update', compact('updatingArticle'));
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/login');
        }
    }

    /**
     * @return void
     */
    public function create(): void
    {
        ArticleModel::created($_POST);
        header('Location: ' . $_ENV['APP_URL'] . '/article/sort/' . $_POST['sort_id']);
    }

    /**
     * @param array<int> $params
     * @return void
     */
    public function update(array $params): void
    {
        ArticleModel::updated($_POST, ['id' => $params[0]]);
        header('Location: ' . $_ENV['APP_URL'] . '/user/article/list');
    }

    /**
     * @param array<int> $params
     * @return void
     */
    public function delete(array $params): void
    {
        ArticleModel::deleted(['id' => $params[0]]);
        header('Location: ' . $_ENV['APP_URL'] . '/user/article/list');
    }

    /**
     * @param array<int> $params
     * @return boolean|null
     */
    public function order(array $params = []): bool|null
    {
        if (isset($_SESSION['id'])) {
            $title = 'Ordered';

            $this->set(compact('title'));
            if ($params) {
                $sortId = $params[0];
                $articles = ArticleModel::ordered($_POST['order_by'], $_POST['order'], ['sort_id' => $sortId], 'articles.id, title, description, sort, sort_id, user_id, articles.created_at, email, full_name', 'JOIN users ON articles.user_id = users.id JOIN sorts ON articles.sort_id = sorts.id');
                return $this->view('article/list', compact('articles', 'sortId'));
            } else {
                $articles = ArticleModel::ordered($_POST['order_by'], $_POST['order'], ['user_id' => $_SESSION['id']], 'articles.id, title, description, sort, sort_id, user_id, articles.created_at, email, full_name', 'JOIN users ON articles.user_id = users.id JOIN sorts ON articles.sort_id = sorts.id');
                return $this->view('article/list', compact('articles'));
            }
        } else {
            header('Location: ' . $_ENV['APP_URL'] . '/login');
        }
    }
}
