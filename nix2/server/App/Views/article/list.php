<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<?php
if (isset($sortId)) {
    echo '<h1>Articles of sort ' . $articles[0]['sort'] . '</h1>';
} else {
    $sortId = '';
    echo '<h1>Your articles</h1>';
}
foreach ($articles as $index => $item) :
    ?>

    <table width="528" border="0">
        <tr>
            <th height="31" scope="col" colspan="2" align="left">
                <a href="/article/list/<?php echo $item['id'] ?>"><?php echo $item['title'] ?></a>
            </th>
        </tr>
        <tr>
            <td height="100%" width="50%" scope="col">Author: <?php echo $item['full_name'] ?></td>
            <td height="100%" width="50%" scope="col">Published: <?php echo $item['created_at'] ?></td>
        </tr>
    </table>
    <br>
    <?php
endforeach;
?>
<form method="post" action="/article/order/<?php echo $sortId?>">
    <div class="d-flex justify-content-start flex-column">
        <div class="p-2">
            <button type="submit" id="buttonUpdate" class="btn btn-primary orderArticles">Order by</button>
        </div>
        <div class="p-2">
            <div class="form-floating">
                <select name="order_by" class="form-select" id="floatingSelect"
                        aria-label="Floating label select example" required>
                    <option name="order_by" value="title">Title</option>
                    <option name="order_by" value="created_at">Create date</option>
                    <option name="order_by" value="full_name">Author</option>
                </select>
                <label for="floatingSelect"> Order by </label>
            </div>
        </div>
        <div class="p=2">
            <div class="form-floating">
                <select name="order" class="form-select" id="floatingSelect"
                        aria-label="Floating label select example" required>
                    <option name="order" value="asc">ASC</option>
                    <option name="order" value="desc">DESC</option>
                </select>
                <label for="floatingSelect">Order </label>
            </div>
        </div>
    </div>
</form>
<script src="/js/jquery-3.6.0.min.js"></script>
<script>
</script>
</body>
<link rel="stylesheet" href="/css/articleList.css">

</html>
