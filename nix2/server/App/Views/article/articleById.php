<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/articleById.css">
    <title>Document</title>
</head>
<body>
<h1>
    <span><?php echo $articleById[0]['title'] /** @phpstan-ignore-line */ ?></span>
</h1>
<div class="info">
    <p class="date">
        <time><?php echo $articleById[0]['created_at']?></time>
    </p>
    <p class="author">Author: <?php echo $articleById[0]['full_name']?></p>
</div>
<div class="text">
    <p>
        <?php echo $articleById[0]['description']?>
    </p>
    <p>
        Sort: <?php echo $articleById[0]['sort']?>
    </p>
</div>
<?php if ($_SESSION['id'] == $articleById[0]['user_id']) : ?>
<div>
    <div class="text">
        <p><a href="/article/list/update/<?php echo $articleById[0]['id']?>">Update</a>
            <a href="/article/delete/<?php echo $articleById[0]['id']?>">Delete</a>
        </p>
    </div>
</div>
<?php endif;?>
<style>

</style>
</html>
