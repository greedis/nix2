<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Write</title>
</head>
<body>

<form action="/article/create" method="post">
    <h2>Write article for <?php echo $_SESSION['full_name']?></h2>
    <div class="form-floating mb-3">
        <input type="text" name="title" class="form-control" id="floatingInput" placeholder="Title">
        <label for="floatingInput">Title</label>
    </div>
    <div class="form-floating">
        <textarea name="description" class="form-control" placeholder="Description" id="floatingTextarea2" style="height: 100px"></textarea>
        <label for="floatingTextarea2">Description</label>
    </div>
    <div class="form-floating">
        <select name="sort_id" class="form-select" id="floatingSelect" aria-label="Floating label select example" required>
            <option name="sort_id" value="1">Informatics</option>
            <option name="sort_id" value="2">Physics</option>
            <option name="sort_id" value="3">Maths</option>
            <option name="sort_id" value="4">Chemistry</option>
            <option name="sort_id" value="5">History</option>
            <option name="sort_id" value="6">Biology</option>
        </select>
        <label for="floatingSelect">Sort: </label>
    </div>
    <input type="hidden" name="user_id" value="<?php echo $_SESSION['id']?>">
    <button type="submit" class="btn btn-primary">Create article</button>
</form>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

</body>
<style>
    a {
        color: #7c9ab7;
        font-weight: bold;
        text-decoration: none;
    }
</style>
</html>
