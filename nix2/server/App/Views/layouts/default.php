<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/default.css">
    <title><?php echo $title ?? 'default' ?></title>
</head>
<body>
<?php if (isset($_SESSION['id'])) { ?>
<header>
    <div>
        <table class="header">
            <tr><td>
                    <a href="/list"><img class="avatar" src="<?php echo $_SESSION['avatar']?>"> <?php echo $_SESSION['email']?></a>
                    <a href="/article/writeArticle"> Write article</a>
                </td>
                <td align="left"></td>
                <td><?php echo $_SESSION['full_name']?></td>
                <td><a class="logout" href="/update">Options</a></td>
               <td><a class="logout" href="/logout">Logout</a></td>
            </tr>
        </table>
    </div>
</header>
<?php }?>
</body>
</html>
