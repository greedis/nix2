<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>List</title>
</head>
<body>
<form>
    <div class="d-flex flex-row">
        <div class="p-2">
            <button type="submit" id="buttonUpdate" class="btn btn-primary allArticles">Articles by</button>
        </div>
        <div class="p-2">
            <div class="form-floating">
                <select name="sort_id" class="form-select" id="floatingSelect"
                        aria-label="Floating label select example" required>
                    <option name="sort_id" value="1">Informatics</option>
                    <option name="sort_id" value="2">Maths</option>
                    <option name="sort_id" value="3">Physics</option>
                    <option name="sort_id" value="4">Chemistry</option>
                    <option name="sort_id" value="5">History</option>
                    <option name="sort_id" value="6">Biology</option>
                </select>
                <label for="floatingSelect">Sort: </label>
            </div>
        </div>
        <div class="p=2">
            <button type="submit" id="buttonUpdate" class="btn btn-primary myArticles"> My articles</button>
        </div>
    </div>
    <p class="msg none"></p>
</form>
<script src="/js/jquery-3.6.0.min.js"></script>
<script>
    $('.myArticles').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/user/articles',
            type: 'POST',
            dataType: 'json',
            data: {},
            success: function (data) {
                if (data.status) {
                    document.location.href = '/user/article/list';
                } else {
                    $('.msg').removeClass('none').text(data.message);
                }
            }
        });
    })
    $('.allArticles').click(function (e) {
        e.preventDefault();
        let sort_id = $('select[name="sort_id"]').val();
        $.ajax({
            url: '/articles',
            type: 'POST',
            dataType: 'json',
            data: {
                sort_id: sort_id
            },
            success: function (data) {
                if (data.status) {
                    document.location.href = "/article/sort/" + data.sort_id;
                } else {
                    $('.msg').removeClass('none').text(data.message);
                }
            }
        });
    })
</script>
</body>
<style>
    .msg {
        border: 2px solid #ffa908;
        border-radius: 3px;
        padding: 10px;
        text-align: center;
        font-weight: bold;
    }

    p {
        margin: 10px 0;
    }

    .allArticles{
        height: 60px;
    }
    
    .myArticles {
        height: 60px;
    margin: 8px;
    }

    .none {
        display: none;
    }

    a {
        color: #7c9ab7;
        font-weight: bold;
        text-decoration: none;
    }
</style>
</html>
