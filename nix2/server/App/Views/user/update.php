<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSS only -->
    <link href="/css/login.css" rel="stylesheet">
    <title>Update</title>
</head>
<body>
<?php
$user = $_SESSION;
?>
<form class="form" enctype="multipart/form-data">
    <label for="floatingInput1">Full name</label>
    <input type="text" name="full_name" value="<?php echo $user['full_name'] ?>" class="form-control" id="floatingInput1"
           placeholder="Enter your full name" required>
    <label for="floatingInput">Email address</label>
    <input type="email" value="<?php echo $user['email'] ?>" name="email" class="form-control" id="floatingInput"
           placeholder="Enter your email">
    <label>Profile image</label>
    <input type="file" name="avatar" required   >
    <label for="floatingPassword1">Password</label>
    <input type="password" name="password" class="form-control" id="floatingPassword1" placeholder="Password">
    <label for="floatingPassword">Confirm password</label>
    <input type="password" name="confirmPassword" class="form-control" id="floatingPassword"
           placeholder="Confirm password">
    <label for="floatingSelect">Gender</label>
    <select name="gender_id" class="form-select" id="floatingSelect" aria-label="Floating label select example">
        <option name="gender_id" value="0">Not known</option>
        <option name="gender_id" value="1">Male</option>
        <option name="gender_id" value="2">Female</option>
        <option name="gender_id" value="9">Not applicable</option>
    </select>
    <button type="submit" class="update-btn">Update user</button>
    <br>

    <p class="msg none"></p>
</form>
<button><a href="/user/delete">Delete user</a></button>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    let avatar = "<?php echo $_SESSION['avatar']?>";
    $('input[name="avatar"]').change(function (e) {
        avatar = e.target.files[0];
    });
    $('.update-btn').click(function (e) {
        e.preventDefault();

        let full_name = $('input[name="full_name"]').val(),
            email = $('input[name="email"]').val(),
            password = $('input[name="password"]').val(),
            confirmPassword = $('input[name="confirmPassword"]').val(),
            gender_id = $('select[name="gender_id"]').val();

        let formData = new FormData();
        formData.append('full_name', full_name);
        formData.append('email', email);
        formData.append('password', password);
        formData.append('confirmPassword', confirmPassword);
        formData.append('gender_id', gender_id);
        formData.append('avatar', avatar);
        $.ajax({
            url: '/user/update',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success(data) {
                if (data.status) {
                    document.location.href = '/list';
                } else {
                    $('.msg').removeClass('none').text(data.message);
                }
            }
        });
    })
</script>

</body>
</html>
