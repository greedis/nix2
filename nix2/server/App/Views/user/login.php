<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Authorization</title>
    <link rel="stylesheet" href="/css/login.css">
</head>
<body>
<form class="form">
    <h1><?php echo $text?></h1>
    <label>Email</label>
    <input type="text" name="email" placeholder="Enter your email" required>
    <label>Password</label>
    <input type="password" name="password" placeholder="Enter your password" required>
    <button type="submit" class="login-btn">Sign in</button>
    <p>
        Don't have an account? - <a href="/register">register</a>!
    </p>
        <p class="msg none"></p>
</form>
<script src="/js/jquery-3.6.0.min.js"></script>
<script>
    $('.login-btn').click(function (e) {
        e.preventDefault();

        let email = $('input[name="email"]').val(),
            password = $('input[name="password"]').val();

        $.ajax({
            url: '/user',
            type: 'POST',
            dataType: 'json',
            data:{
                email: email,
                password:password
            },
            success: function (data) {
                if (data.status){
                    document.location.href = '/list';
                } else {
                    $('.msg').removeClass('none').text(data.message);
                }
            }
        });
    })
</script>
</body>
</html>
