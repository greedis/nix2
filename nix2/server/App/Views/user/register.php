<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSS only -->
    <link href="/css/login.css" rel="stylesheet">
    <title>Register</title>
</head>
<body>
<form enctype="multipart/form-data">
    <h1><?php echo $text?></h1>
    <label>Full name</label>
    <input type="text" name="full_name" placeholder="Enter your full name">
    <label>Email address</label>
    <input type="email" name="email" placeholder="name@example.com">
    <label>Profile image</label>
    <input type="file" name="avatar">
    <label>Password</label>
    <input type="password" name="password" placeholder="Password">
    <label >Confirm password</label>
    <input type="password" name="confirmPassword" placeholder="Password"
           required>
    <label>Choose your gender</label>
    <select name="gender_id" aria-label="Floating label select example">
        <option name="gender_id" value="0">Not known</option>
        <option name="gender_id" value="1">Male</option>
        <option name="gender_id" value="2">Female</option>
        <option name="gender_id" value="9">Not applicable</option>
    </select>
    <br>
    <button type='submit' class="btn btn-primary">Register</button>
    <p>
        Already have an account? - <a href="/login">Sign in</a>!
    </p>
    <p class="msg none"></p>
</form>
<script src="/js/jquery-3.6.0.min.js"></script>
<script>

    let avatar = false;

    $('input[name="avatar"]').change(function (e) {
        avatar = e.target.files[0];
    });
    $('.btn-primary').click(function (e) {
        e.preventDefault();
        $(`input`).removeClass('error');
        let full_name = $('input[name="full_name"]').val(),
            email = $('input[name="email"]').val(),
            password = $('input[name="password"]').val(),
            confirmPassword = $('input[name="confirmPassword"]').val(),
            gender_id = $('select[name="gender_id"]').val();

        let formData = new FormData();
        formData.append('full_name', full_name);
        formData.append('email', email);
        formData.append('password', password);
        formData.append('confirmPassword', confirmPassword);
        formData.append('gender_id', gender_id);
        formData.append('avatar', avatar);
        $.ajax({
            url: '/user/create',
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            success(data) {
                if (data.status) {
                    document.location.href = '/login';
                } else {
                    $('.msg').removeClass('none').text(data.message);
                }
            }
        });
    });
</script>

</body>
</html>
