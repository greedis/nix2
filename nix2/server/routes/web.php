<?php

use App\Controllers\ArticleController;
use App\Controllers\MainController;
use App\Controllers\UserController;
use DolgoyAudiopunk\Framework\Route;

Route::add('/article/list/(\d+)', [ArticleController::class, 'getArticleById/$1']);
Route::add('/article/list/update/(\d+)', [ArticleController::class, 'updateArticle/$1']);
Route::add('/article/update/(\d+)', [ArticleController::class, 'update/$1']);

Route::add('/', [MainController::class, 'index']);
Route::add('/logout', [MainController::class, 'logOut']);

Route::add('/register', [UserController::class, 'register']);
Route::add('/login', [UserController::class, 'login']);
Route::add('/update', [UserController::class, 'updateUser']);

Route::add('/user', [UserController::class, 'index']);
Route::add('/list', [UserController::class, 'list']);

Route::add('/user/create', [UserController::class, 'create']);
Route::add('/user/update', [UserController::class, 'update']);
Route::add('/user/delete', [UserController::class, 'delete']);

Route::add('/article/sort/(\d+)', [ArticleController::class, 'articlesBySort/$1']);
Route::add('/user/article/list', [ArticleController::class, 'viewUserArticles']);
Route::add('/user/articles', [ArticleController::class, 'userArticles']);
Route::add('/articles', [ArticleController::class, 'sortArticles']);

Route::add('/article/writeArticle', [ArticleController::class, 'writeArticle']);

Route::add('/article/create', [ArticleController::class, 'create']);

Route::add('/article/delete/(\d+)', [ArticleController::class, 'delete/$1']);
Route::add('/article/order/(\d+)', [ArticleController::class, 'order/$1']);
Route::add('/article/order/', [ArticleController::class, 'order']);
