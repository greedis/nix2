<?php

use JetBrains\PhpStorm\NoReturn;

/**
 * @param mixed $array
 * @return void
 */
function dump(mixed $array):void
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

/**
 * @param mixed $array
 * @return void
 */
function dd(mixed $array):void
{
    dump($array);
    die();
}

/**
 * @param array<int|string> $array
 * @param string            $operator
 * @return array<int|string>
 */
function convArray(array $array, string $operator = '='): array
{
    foreach ($array as $key => $item) {
        if ($key == 'id') {
            $array[$key] = "$key $operator $item";
        } else {
            $array[$key] = "$key $operator '$item'";
        }
    }
    return $array;
}
