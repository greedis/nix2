<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<h2>Registration form</h2>
<form id="ajax_form" action="insertInUsers.php" method="post">
    <div class="input-group mb-3">
        <span class="input-group-text" id="basic-addon1">First name:</span>
        <input type="text" name="first_name" class="form-control" placeholder="First name" aria-label="First name" aria-describedby="basic-addon1">
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text" id="basic-addon1">Last name:</span>
        <input type="text" name="last_name" class="form-control" placeholder="Last name" aria-label="Last name" aria-describedby="basic-addon1">
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text" id="basic-addon1">Phone number</span>
        <input type="text" name="phone_number" class="form-control" placeholder="Phone number" aria-label="Phone number" aria-describedby="basic-addon1">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Email address</label>
        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text" id="basic-addon1">Age:</span>
        <input type="number" name="age" class="form-control" placeholder="Age" aria-label="Age" aria-describedby="basic-addon1">
    </div>
    <button id="send" type="submit" class="btn btn-primary" name="send">Send</button>
</form>
<h2>Review form</h2>
<form id="reviews_form" action="insertInReviews.php" method="post">
    <div class="input-group mb-3">
        <span class="input-group-text" id="basic-addon1">Username:</span>
        <input type="text" name="username" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
    </div>
    <div class="input-group mb-3">
        <span class="input-group-text" id="basic-addon1">Review:</span>
        <input type="text" name="review" class="form-control" placeholder="Review" aria-label="Review" aria-describedby="basic-addon1">
    </div>
    <button id="sendReview" type="submit" class="btn btn-primary" name="sendReview">Send review</button>
</form>
<div class="btn-group">
    <button id="loadUsers" name="loadUsers" class="btn btn-primary">Load users</button>
    <button id="loadUsersAsc" name="loadUsersAsc" class="btn btn-primary">Load users asc</button>
    <button id="loadUsersDesk" name="loadUsersDesk" class="btn btn-primary">Load users desk</button>
</div>
<div class="container">
    <div id="result"></div>
</div>
<div class="container">
    <button id="loadReviews" class="btn btn-primary" name="loadReviews">Load Reviews</button>
</div>
<div class="container">
    <div id="result2"></div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="script.js"></script>
</body>
</html>
