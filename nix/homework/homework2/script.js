$(document).ready(function (){
    $('#loadUsers').on('click', function (e){
        e.preventDefault()
        fetch('http://localhost:8184/homework2/getUsers.php').then(function (response){
            response.json().then(function (data){
                $("#result").html(JSON.stringify(data))
            })
            // console.log()
        })
    })
    $('#loadReviews').on('click', function (c){
        c.preventDefault()
        fetch('http://localhost:8184/homework2/getReviews.php').then(function (response){
            response.json().then(function (data){
                $("#result2").html(JSON.stringify(data))
            })
        })
    })
    $('#loadUsersAsc').on('click', function (c){
        c.preventDefault()
        fetch('http://localhost:8184/homework2/getUsersAsc.php').then(function (response){
            data = JSON.parse(data);
            let len = data.length;
            let list_li = '';
            for (let i = 0; i < len; i++) {
                list_li += `<li>${data[i]['first_name']}</li>`;
                list_li += `<li>${data[i]['last_name']}</li>`;
                list_li += `<li>${data[i]['phone_number']}</li>`;
                list_li += `<li>${data[i]['email']}</li>`;
                list_li += `<li>${data[i]['age']}</li>`;
            }
            $("#result").html(`<ul>${list_li}</ul>`);

        })
    })
    $('#loadUsersDesk').on('click', function (c){
        c.preventDefault()
        fetch('http://localhost:8184/homework2/getUsersDesk.php').then(function (response){
            response.json().then(function (data){
                $("#result").html(JSON.stringify(data))
            })
        })
    })
    $('form').on('submit', function (event) {
        event.preventDefault()
        $.ajax({
            url: $(this).attr("action"),
            method: $(this).attr("method"),
            data: $(this).serialize(),
            dataType: "html",
            success: function (data) {
                $("#result").html(data);
                console.log('success')
            },
            beforeSend: function () {
                console.log('beforeSend')
            },
            error: function () {
                console.log('error')
            },
            complete: function () {
                console.log('complete')
            }
        });

        let q = $('input')
        for (const $formElement of q) {
            $formElement.value = ''
        }
    })
})
