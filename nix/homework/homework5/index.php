<?php
require_once '../../../vendor/autoload.php';

class SpaceObjects
{

}

class Planet extends SpaceObjects
{
    public string $size;
    public string $weight;
    public bool $existingOfLife;
    public int $countOfSatellites;
    public string $temperature;

    public function __construct($size, $weight, $existingOfLife, $countOfSatellites, $temperature)
    {
        $this->size = $size;
        $this->weight = $weight;
        $this->existingOfLife = $existingOfLife;
        $this->countOfSatellites = $countOfSatellites;
        $this->temperature = $temperature;
    }

    public function setSize(string $size): void
    {
        $this->size = $size;
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function setWeight(string $weight): void
    {
        $this->weight = $weight;
    }

    public function getWeight(): string
    {
        return $this->weight;
    }

    public function setExistingOfLife(string $existingOfLife): void
    {
        $this->existingOfLife = $existingOfLife;
    }

    public function getExistingOfLife(): string
    {
        return $this->existingOfLife;
    }

    public function setCountOfSatellites(string $countOfSatellites): void
    {
        $this->countOfSatellites = $countOfSatellites;
    }

    public function getCountOfSatellites(): string
    {
        return $this->countOfSatellites;
    }

    public function setTemperature(string $temperature): void
    {
        $this->temperature = $temperature;
    }

    public function getTemperature(): string
    {
        return $this->temperature;
    }

}

$earth = new Planet('12 742 km', '5,972E24 kg', true, 1, '-89°C to 56°C');

echo 'get_class_methods<br>';
echo '<pre>';
print_r(get_class_methods($earth));
echo '</pre><hr>';

echo 'get_class<br>';
echo '<pre>';
print_r(get_class($earth));
echo '</pre><hr>';

echo 'get_class_vars<br>';
echo '<pre>';
print_r(get_class_vars(get_class($earth)));
echo '</pre><hr>';

echo 'get_object_vars<br>';
echo '<pre>';
print_r(get_object_vars($earth));
echo '</pre><hr>';

echo 'method_exists<br>';
echo '<pre>';
print_r(method_exists($earth, 'getSize'));
echo '</pre><hr>';

echo 'get_parent_class<br>';
echo '<pre>';
print_r(get_parent_class($earth));
echo '</pre><hr>';

echo 'is_subclass_of<br>';
echo '<pre>';
print_r(is_subclass_of($earth, 'SpaceObjects'));
echo '</pre><hr>';

echo 'get_declared_classes<br>';
echo '<pre>';
print_r(get_declared_classes());
echo '</pre><hr>';
