<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Send log</title>
</head>
<body>
<p>Start chat with
    <a href="https://t.me/L0GGGGGer_Bot?start" target="_blank">
        <i>
            <img src="telegram.png" width="1%">
        </i>
        Logger bot</a>.
</p>
<form id="ajax_form" action="sendLog.php" method="post">
    <button id="send" type="submit" class="btn btn-primary" name="sendLog">Send log</button>
</form>
</body>
</html>