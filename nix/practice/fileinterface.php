<?php
interface FileInterface {
    public function readFromFile($path);
    public function writeToFile($path, $some);
}