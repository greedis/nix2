<?php
require_once '../../vendor/autoload.php';
require_once 'fileinterface.php';
require_once 'client.php';

class Shop implements FileInterface, Client
{

    public function buy($id)
    {
        echo "Спасибо за покупку<br />";
        $this->writeToFile("data.db", "Был куплен товар $id");    }

    public function repayment($id)
    {
        $this->readFromFile("data.db");
        //Тут, допустим, проверка того, была ли на самом деле покупка товара $id
        $this->writeToFile("data.db", "Был сделан возврат товара $id");    }

    public function readFromFile($path)
    {
        echo "Считываем из файла и возвращаем строку<br />";
    }

    public function writeToFile($path, $some)
    {
        echo "Записываем в файл данные $some<br />";
    }
}

$shop = new Shop();
$shop->buy(5);
$shop->repayment(5);