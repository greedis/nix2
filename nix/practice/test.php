<?php
$string = '30-07-2022';

$pattern = '/(\d{2})-(\d{2})-(\d{4})/';

$replacement = 'Month: $2, Day: $1, Year: $3';

echo preg_replace($pattern, $replacement, $string);